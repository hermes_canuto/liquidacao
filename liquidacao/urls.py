from django.urls import path, include
from rest_framework.documentation import include_docs_urls

DESCRICAO = '''

# Olá, esta é a documentação da api de liquidação da **Lucree**

# autenticação

Para utilizar os métodos listados abaixo, é necessário realizar uma autenticação via Tokent JWT.

Para realizar a autenticação realize os seguintes passos:

__POST__

```
/autenticacao/v1
```

__Body:__

```
{
  "username": "seu.usuario",
  "password": "sua.senha"
}
```

OBS: A senha deve conter letras + numero + caractere especial. Seu usuário será criado quando você realizar o login.

O retorno conterá a chave __"id_token"__, que é o JWT utilizado para as requisições nos demais endpoints.

Após isso basta passar o seguinte parametro no header:

```
Header: Authorization <id_token>
```

'''

urlpatterns = [
    path('', include_docs_urls(title='Liquidação Lucree',
                               description=DESCRICAO,
                               authentication_classes=[],
                               permission_classes=[])),
    path('', include('api.urls'))
]
