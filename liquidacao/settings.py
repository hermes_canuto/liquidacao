import os


# Static Config
S3_BUCKET = os.environ.get('S3_BUCKET')
AWS_S3_REGION_NAME = os.environ.get('AWS_S3_REGION_NAME')
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')

# AWS_ACCESS_KEY_ID = "AKIASRYHOQLBBIUKVSP6"
# AWS_SECRET_ACCESS_KEY = "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"
# S3_BUCKET = "swagger-static"
# AWS_S3_REGION_NAME = "us-west-2"

STATICFILES_STORAGE = "django_s3_storage.storage.StaticS3Storage"
AWS_STORAGE_BUCKET_NAME = S3_BUCKET
AWS_S3_BUCKET_NAME_STATIC = S3_BUCKET
AWS_S3_CUSTOM_DOMAIN = '%s.s3.us-west-2.amazonaws.com' % S3_BUCKET
STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN
AWS_DEFAULT_ACL = None


# RDS Config
RDS_DB_NAME = os.environ.get('RDS_DB_NAME')
RDS_USERNAME = os.environ.get('RDS_USERNAME')
RDS_PASSWORD = os.environ.get('RDS_PASSWORD')
RDS_HOSTNAME = os.environ.get('RDS_HOSTNAME')
# RDS_DB_NAME = "subadiqdb1"
# RDS_USERNAME = "subadiqdb"
# RDS_PASSWORD = "1o5gHogS"
# RDS_HOSTNAME = "subadiq-homolog.cxmg4sq9azs9.us-west-2.rds.amazonaws.com"

# External APIs Config
SKIPPER_FEES_URL = os.environ.get('SKIPPER_FEES_URL')
SKIPPER_USER = os.environ.get('SKIPPER_USER')
SKIPPER_PASS = os.environ.get('SKIPPER_PASS')


# SKIPPER_FEES_URL = "https://api-liq-skipper.lucree.com.br/settlement/v2/fees/"
# SKIPPER_USER = "admin@4ward.com.br"
# SKIPPER_PASS = "12345678"


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Secured Secret Key
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECRET_KEY = "evv9j60w=i)mu93x!mz-^%32jmv@^ru5t+(q&!7!e@*s)+&e8p"

DEBUG = False

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'api',
    'rest_framework',
    'rest_framework_swagger',
    'django_s3_storage',
    'storages'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'liquidacao.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'liquidacao.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': RDS_DB_NAME,
        'USER': RDS_USERNAME,
        'PASSWORD': RDS_PASSWORD,
        'HOST': RDS_HOSTNAME,
        'PORT': 5432,
        'OPTIONS': {
            'options': '-c search_path=credenciamento'
        }
    }
}

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(levelname)s] - [%(asctime)s] - %(module)s - %(message)s'
        }
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            },
        },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False,
        },
        'django.request': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False,
        },
        '': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False
        },
    },
}

# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
