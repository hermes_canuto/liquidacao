import logging
import requests
from math import ceil
from requests import Session
from datetime import datetime, timedelta
from liquidacao.settings import SKIPPER_FEES_URL, SKIPPER_USER, SKIPPER_PASS

logger = logging.getLogger(__name__)


class SkipperSession(Session):
    """
    Classe 'Session' parametrizada para a
    consulta de aluguéis no Skipper.

    date: str
        Data de consulta na API do
        Skipper (YYYY-MM-DD). Atualmente,
        a consulta está limitada ao range
        de 1 dia.

    auth: tuple
        Usuário e senha do Skipper.

    params: dict
        Parâmetros default para todas
        as chamadas no endpoint de
        aluguel do Skipper.

    total_fees: list
        Lista com todos os aluguéis
        disponíveis na data pesquisada.
    """

    def __init__(self, date):
        super().__init__()
        self.date = date
        self.auth = (SKIPPER_USER, SKIPPER_PASS)
        self.params = {'startDate': self.start_date,
                       'endDate': self.date,
                       'type': 'ALUGUEL',
                       'pageSize': 100}
        self.total_fees = self.__total_fees()

    @property
    def start_date(self):
        """
        Retorna o dia anterior da
        data enviada como parâmetro,
        limitando ao range de 1 dia
        de consulta.
        """
        try:
            start_date = datetime.strptime(self.date, "%Y-%m-%d")
            start_date = start_date + timedelta(days=-1)
            return start_date.strftime("%Y-%m-%d")
        except (ValueError, TypeError):
            return ''

    def __skipper_request(self):
        """
        Realiza um GET na API do Skipper.
        Retorna os resultado em um dict
        ou um dict vazio em caso de falha.
        """
        try:
            response = self.get(SKIPPER_FEES_URL, timeout=3)
            logger.info("Skipper response status: {}".format(response.status_code))
            if response.status_code != 200:
                return {}
            logger.info(response.json())
            return response.json()
        except requests.exceptions.RequestException as e:
            logger.error("Skipper exception: {}".format(e))
            return {}

    @staticmethod
    def __get_page_numbers(records):
        """
        Retorna a quantidade de páginas
        arredondada para cima para ser iterada.
        """
        if records < 101:
            return 1
        return ceil(records/100)

    def __total_fees(self):
        """
        Retorna uma lista contendo todas
        as páginas de resultado da API
        de aluguel do Skipper agrupadas.

        Caso o Skipper esteja down, retorna
        None, caso o 'totalNumberOfRecords'
        seja 0, retorna 0.
        """
        fees = []
        response = self.__skipper_request()
        if not response:
            return None

        try:
            if not response['pageInfo']['totalNumberOfRecords']:
                logger.info("No rental data for this date")
                return 0
            page_numbers = self.__get_page_numbers(response['pageInfo']['totalNumberOfRecords'])
        except KeyError:
            return None

        try:
            fees.append(response['fees'])
        except KeyError:
            return 0

        logger.info("Skipper page numbers: {}".format(page_numbers))
        if page_numbers > 1:
            for i in range(1, page_numbers):
                self.params['page'] = i + 1
                r = self.__skipper_request()
                fees.append(r['fees'])

        return fees

    @staticmethod
    def __filter_by_cnpj(fees, cnpj):
        """
        Filtra os aluguéis encontrados
        pelo CNPJ recebido.
        """
        cnpj_fees = []
        try:
            for fee in fees:
                for f in fee:
                    if f['customer']['document']['number'] == cnpj:
                        cnpj_fees.append(f)
            return cnpj_fees
        except KeyError:
            return []

    @staticmethod
    def __rental_value(fees):
        """
        Retorna o valor total de todos
        os aluguéis encontrados que estejam
        com o status 'Pago'.
        """
        rental_value = 0
        try:
            for fee in fees:
                if fee.get('status', 0) != 'Pago':
                    continue
                rental_value += fee['originalValue']
            return round(rental_value, ndigits=2)
        except KeyError:
            return rental_value

    def get_rental_value(self, cnpj):
        """
        Retorna 'Aluguel indisponível'
        caso o Skipper esteja down, '0'
        caso não haja aluguéis para a data
        especificada ou o valor total de
        aluguéis calculado.
        """
        if self.total_fees is None:
            return 'Aluguel indisponível'
        elif self.total_fees == 0:
            return self.total_fees

        cnpj_fees = self.__filter_by_cnpj(self.total_fees, cnpj)
        return self.__rental_value(cnpj_fees)
