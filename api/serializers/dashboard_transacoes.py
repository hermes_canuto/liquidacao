from rest_framework import serializers
from api.models import DashboardTransacoes


class DashboardTransacoesSerializer(serializers.ModelSerializer):

    class Meta:
        model = DashboardTransacoes
        fields = '__all__'
