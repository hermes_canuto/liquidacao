from rest_framework import serializers
from api.models import DashboardLiquidacao


class DashboardLiquidacaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = DashboardLiquidacao
        fields = ("id",
                  "nsu",
                  "acquirer_nsu",
                  "codautorizacao",
                  "terminal",
                  "dataprevista",
                  "valorliquido",
                  "valorbruto",
                  "nro_parcela",
                  "status",)
