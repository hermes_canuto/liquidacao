from django.utils.timezone import is_aware, make_aware
from django.utils.dateparse import parse_datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from api.models import AgendaFinanceira, DashboardTransacoes


class Transacoes(APIView):
    """
    get:
    Retorna todas as transações contidas entre as datas especificadas.

    ### Path Parameters

    The following parameters should be included in the URL path.

    `transacoes/?start=<AAAA-MM-DD>T<HH:MM:SS>&end=<AAAA-MM-DD>T<HH:MM:SS>`

    ### Ex:
    `transacoes/?start=2019-04-12T00:00:01&end=2019-04-12T23:59:59`

    """
    def get(self, request):
        start_date = request.query_params.get('start', None)
        finish_date = request.query_params.get('end', None)

        try:
            t_queryset = DashboardTransacoes.objects.filter(confirmation_date__gte=get_aware_datetime(start_date),
                                                            confirmation_date__lte=get_aware_datetime(finish_date)).values()
        except ValueError:
            return Response({'Error': 'Formato de data invalido'}, status=status.HTTP_400_BAD_REQUEST)

        result = []
        for t in t_queryset:
            l_queryset = AgendaFinanceira.objects.values(
                "nsu",
                "codautorizacao",
                "terminal",
                "dataprevista",
                "valorliquido",
                "valorbruto",
                "nro_parcela",
                "status"
                ).filter(terminal=t['tef_terminal'],
                         nsu=t['nsu'],
                         cnpj_cpf=t['merchant_national_id'],
                         codautorizacao=t['authorization_number'],
                         valortransacao=t['value'])
            t['schedules'] = l_queryset
            result.append(t)
        return Response(result, status=status.HTTP_200_OK)


def get_aware_datetime(date_str):
    try:
        ret = parse_datetime(date_str)
        if not is_aware(ret):
            ret = make_aware(ret)
        return ret
    except (TypeError, AttributeError):
        return None
