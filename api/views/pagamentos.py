from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
import pandas as pd
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
from ..models import AgendaSkipper, DocumentoExcel, ConciliacaoSkipper
from datetime import datetime

def add_data_pagamento(vl_bruto, vl_desc, vl_antecip, vl_liq, nsu, dt_trans, parc, dt_sched, autoriz, prod_trans, bandeira, lote_id, data_pagto, tipo, lojista):

    split_parc = parc.split('/')
    curr_parc = 0
    total_parc = 0

    if len(split_parc) == 2:
        curr_parc = split_parc[0]
        total_parc = split_parc[1]

    agenda = None

    if tipo == 'agenda':
        agenda = AgendaSkipper.objects.filter(
            lote_id=lote_id,
            data_pagto=datetime.strptime(data_pagto, '%d/%m/%Y'),
            vl_bruto=vl_bruto,
            vl_liq=vl_liq,
            nsu=str(nsu).zfill(12),
            parc=curr_parc,
            autoriz=str(autoriz).zfill(6),
            dt_trans=datetime.strptime(dt_trans, '%d/%m/%Y')).first()
    else:
        agenda = ConciliacaoSkipper.objects.filter(
            lote_id=lote_id,
            data_pagto=datetime.strptime(data_pagto, '%d/%m/%Y'),
            vl_bruto=vl_bruto,
            vl_liq=vl_liq,
            nsu=str(nsu).zfill(12),
            parc=curr_parc,
            autoriz=str(autoriz).zfill(6),
            dt_trans=datetime.strptime(dt_trans, '%d/%m/%Y'),
            lojista=lojista).first()

    if agenda is None:
        if tipo == 'agenda':
            agenda = AgendaSkipper(
                lote_id=lote_id,
                vl_bruto=vl_bruto,
                vl_desc=vl_desc,
                vl_antecip=vl_antecip,
                vl_liq=vl_liq,
                nsu=str(nsu).zfill(12),
                dt_trans=datetime.strptime(dt_trans, '%d/%m/%Y'),
                parc=curr_parc,
                total_parc=total_parc,
                dt_sched=datetime.strptime(dt_sched, '%d/%m/%Y'),
                autoriz=str(autoriz).zfill(6),
                prod_trans=prod_trans,
                bandeira=bandeira,
                data_pagto=datetime.strptime(data_pagto, '%d/%m/%Y')
            )
            agenda.save()
            return True
        else:
            conciliacao = ConciliacaoSkipper(
                lote_id=lote_id,
                vl_bruto=vl_bruto,
                vl_desc=vl_desc,
                vl_antecip=vl_antecip,
                vl_liq=vl_liq,
                nsu=str(nsu).zfill(12),
                dt_trans=datetime.strptime(dt_trans, '%d/%m/%Y'),
                parc=curr_parc,
                total_parc=total_parc,
                dt_sched=datetime.strptime(dt_sched, '%d/%m/%Y'),
                autoriz=str(autoriz).zfill(6),
                prod_trans=prod_trans,
                bandeira=bandeira,
                data_pagto=datetime.strptime(data_pagto, '%d/%m/%Y'),
                lojista=lojista
            )
            conciliacao.save()
            return True

    return False


def importa(arquivo, tipo):

    columns = [
            'Item', # Itens
            'Cliente',
            'Nome',
            'Adquirente',
            'Vl Liq CR',
            'Vl Liq DB',
            'Vl Liq PAY',
            'Forma Pagto',
            'Banco',
            'Agencia',
            'Conta',
            'Status',
            'Lancto', # Transações
            'Vl Bruto',
            'Vl Desc',
            'Vl Antecip',
            'Vl Liq',
            'Tipo',
            'NSU',
            'Dt Trans', # Linha 2 contem data de pagamento
            'Parc',
            'Dt Sched',
            'Autoriz',
            'Prod Trans',
            'Bandeira',
            'NSU Orig',
            'Dt Trans Orig'
        ]

    # arquivo = r'C:\Users\RenatoAloi\Downloads\PaymentLot-4.xlsx'

    data = pd.read_excel(arquivo, sheet_name='tab1', header=None)
    df = pd.DataFrame(data) # , columns=columns)
    df = df.fillna('')
    readxls(df, tipo)

def readxls(df, tipo):
    a = 0
    processa_proxima_linha = False
    data_pagto = ''
    lote_id = 0
    lojista = ''

    label_pesquisa = 'BAIXADO' if tipo == 'agenda' else 'EM ABERTO'

    for index, row in df.iterrows():
        a = a + 1

        if a == 2: # na linha 2 tem a data do pagamento
            data_pagto = row[19]
            lote_id = int(row[10])
            print('Data Pagto: {} - Lote: {}'.format(str(data_pagto), lote_id))
            print('-' * 100)

        if a > 5:

            if row[11] == label_pesquisa:
                lojista = row[2]
                processa_proxima_linha = True
                continue
            elif row[11] != '':
                processa_proxima_linha = False
                continue

            data_transacao = row[19]
            if row[12] == 'DB':
                data_transacao = datetime.now().strftime('%d/%m/%Y')

            if (processa_proxima_linha and row[12] == 'CR') or \
                (processa_proxima_linha and row[12] == 'DB' and tipo == 'conciliacao'):

                if (row[17] != 'AJ'):
                    ok = add_data_pagamento(
                        vl_bruto=row[13],
                        vl_desc=row[14],
                        vl_antecip=row[15],
                        vl_liq=row[16],
                        nsu=row[18],
                        dt_trans=data_transacao,
                        parc=row[20],
                        dt_sched=row[21],
                        autoriz=row[22],
                        prod_trans=row[23],
                        bandeira=row[24],
                        lote_id=lote_id,
                        data_pagto=data_pagto,
                        tipo=tipo,
                        lojista=lojista
                    )

                    print(
                        a,
                        row[18],
                        row[13],
                        row[19],
                        row[20],
                        row[22],
                        'importei' if ok else 'pulei'
                    )

            # endif processa_proxima_linha


@csrf_exempt
def importa_pagamentos(request):

    if 'arquivo_excel' in request.FILES:

        arquivo_excel = DocumentoExcel(arquivo=request.FILES['arquivo_excel'])
        arquivo_excel.save()
        arquivo = arquivo_excel.arquivo

        importa(arquivo, 'agenda')

    return JsonResponse({ 'result': 'OK' })



@csrf_exempt
def importa_conciliacao(request):

    if 'arquivo_excel' in request.FILES:

        arquivo_excel = DocumentoExcel(arquivo=request.FILES['arquivo_excel'])
        arquivo_excel.save()
        arquivo = arquivo_excel.arquivo

        importa(arquivo, 'conciliacao')

    return JsonResponse({ 'result': 'OK' })
