from api.helpers import SkipperSession
from rest_framework.decorators import action
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Sum, Case, When, FloatField, F, Q
from api.models import DashboardDistribuidores, AgendaFinanceira


class Lotes(APIView):
    """
    get:
    Metodo para obter os lotes de pagamento através da data do lote.
    """

    @action(detail=False, methods=['get'])
    def get(self, request, data):
        response = []
        session = SkipperSession(data)

        distribuidores = DashboardDistribuidores.objects.all()
        for distribuidor in distribuidores:

            list_liquidacoes = []

            liquidacoes = AgendaFinanceira.objects.values(
                'razao_nome', 'cnpj_cpf', 'status'
            ).filter(
                dataprevista=data,
                distribuidor_id=distribuidor.id
            ).annotate(
                valor_liquido_credito=Sum(
                    Case(
                        When(
                            Q(product_id='002') | Q(product_id='001'),
                            then=F('valorliquido')
                        ),
                        default=0,
                        output_field=FloatField()
                    )
                ),
                valor_liquido_debito=Sum(
                    Case(
                        When(
                            product_id='005',
                            then=F('valorliquido')
                        ),
                        default=0,
                        output_field=FloatField()
                    )
                ),
                valor_pagamento=Sum(
                    Case(
                        When(
                            product_id='005',
                            then=F('valorliquido')
                        ),
                        default=0,
                        output_field=FloatField()
                    )
                ) + Sum(
                    Case(
                        When(
                            Q(product_id='002') | Q(product_id='001'),
                            then=F('valorliquido')
                        ),
                        default=0,
                        output_field=FloatField()
                    )
                )
            )

            for liquidacao in liquidacoes:
                detalhe_liquidacao = AgendaFinanceira.objects.values(
                    'valorbruto',
                    'valorliquido',
                    'valoradministrativo',
                    'nsu',
                    'dataautorizacao',
                    'nro_parcela',
                    'dataprevista',
                    'codautorizacao',
                    'produto',
                    'bandeira'
                ).filter(cnpj_cpf=liquidacao['cnpj_cpf'], dataprevista=data)

                liquidacao['valor_aluguel'] = session.get_rental_value(liquidacao['cnpj_cpf'])
                liquidacao['transacoes'] = detalhe_liquidacao
                list_liquidacoes.append(liquidacao)

            dict_distribuidor = {
                'razao_social': distribuidor.razao_social,
                'cnpj': distribuidor.cnpj,
                'itens_lote': list_liquidacoes
            }
            response.append(dict_distribuidor)

        return Response(response, status=status.HTTP_200_OK)
