from django.urls import path
from api.views import Lotes, Transacoes
from api.views.pagamentos import importa_pagamentos, importa_conciliacao

urlpatterns = [
    path('lotes/<str:data>', Lotes.as_view(), name='lotes'),
    path('transacoes/', Transacoes.as_view(), name='transacoes'),
    path('pagamentos/', importa_pagamentos, name='pagamentos'),
    path('conciliacao/', importa_conciliacao, name='conciliacao'),
]
