from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DashboardAtividadessecundaria(models.Model):
    atividade = models.CharField(max_length=250, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    cnpj = models.ForeignKey('DashboardCnpj', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_atividadessecundaria'


class DashboardCertidoesnegativasitens(models.Model):
    emitido_em = models.DateTimeField(blank=True, null=True)
    fonte = models.CharField(max_length=250, blank=True, null=True)
    protocolo = models.CharField(max_length=250, blank=True, null=True)
    url = models.CharField(max_length=250, blank=True, null=True)
    nada_consta = models.CharField(max_length=250, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_certidoesnegativasitens'


class DashboardCessao(models.Model):
    maquina_cessao = models.CharField(max_length=10)
    foto_documento = models.CharField(max_length=400)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)
    descricao = models.TextField(blank=True, null=True)
    status = models.SmallIntegerField(blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_cessao'


class DashboardCnpj(models.Model):
    cnpj = models.CharField(max_length=16, blank=True, null=True)
    nome_fantasia = models.CharField(max_length=150, blank=True, null=True)
    nome_empresarial = models.CharField(max_length=150, blank=True, null=True)
    data_abertura = models.DateField(blank=True, null=True)
    fonte = models.CharField(max_length=50, blank=True, null=True)
    atividade_principal = models.CharField(max_length=300, blank=True, null=True)
    faturamento_presumido = models.CharField(max_length=300, blank=True, null=True)
    numero_funcionarios = models.CharField(max_length=50, blank=True, null=True)
    numero_funcionarios_filiais = models.CharField(max_length=50)
    regime_tributario = models.CharField(max_length=16, blank=True, null=True)
    irs_status = models.CharField(max_length=16, blank=True, null=True)
    capital_social = models.CharField(max_length=16, blank=True, null=True)
    data_situacao_cadastral = models.DateField(blank=True, null=True)
    data_situacao_especial = models.DateField(blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    ente_federativo_responsavel = models.CharField(max_length=300, blank=True, null=True)
    motivo_situacao_cadastral = models.CharField(max_length=250, blank=True, null=True)
    natureza_juridica = models.CharField(max_length=200, blank=True, null=True)
    numero = models.CharField(max_length=16, blank=True, null=True)
    situacao_cadastral = models.CharField(max_length=16, blank=True, null=True)
    situacao_especial = models.CharField(max_length=50, blank=True, null=True)
    telefone = models.CharField(max_length=150, blank=True, null=True)
    tipo = models.CharField(max_length=150, blank=True, null=True)
    localizacao_bairro = models.CharField(max_length=250, blank=True, null=True)
    localizacao_cep = models.CharField(max_length=10, blank=True, null=True)
    localizacao_cidade = models.CharField(max_length=50, blank=True, null=True)
    localizacao_complemento = models.CharField(max_length=150, blank=True, null=True)
    localizacao_estado = models.CharField(max_length=50, blank=True, null=True)
    localizacao_logradouro = models.CharField(max_length=150, blank=True, null=True)
    localizacao_numero = models.CharField(max_length=6, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_cnpj'


class DashboardCpf(models.Model):
    sexo = models.CharField(max_length=2)
    numero = models.CharField(max_length=25)
    data_de_nascimento = models.DateField()
    nome = models.CharField(max_length=300)
    renda = models.CharField(max_length=250)
    pep = models.BooleanField()
    situacao_imposto_de_renda = models.CharField(max_length=250)
    cpf_situacao_cadastral = models.CharField(max_length=250)
    cpf_data_de_inscricao = models.CharField(max_length=250)
    cpf_digito_verificador = models.CharField(max_length=4)
    cpf_anterior_1990 = models.CharField(max_length=1)
    ano_obito = models.CharField(max_length=1)
    grafia = models.CharField(max_length=300)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_cpf'


class DashboardDepartamentos(models.Model):
    codigo_departamento = models.IntegerField(blank=True, null=True)
    descricao_departamento = models.CharField(max_length=250, blank=True, null=True)
    id_mcc = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_departamentos'


class DashboardDistribuidores(models.Model):
    agencia = models.CharField(max_length=10, blank=True, null=True)
    conta = models.CharField(max_length=10)
    digito_conta = models.CharField(max_length=1)
    banco = models.IntegerField(blank=True, null=True)
    tipo_conta = models.CharField(max_length=250)
    cnpj = models.CharField(max_length=250)
    razao_social = models.CharField(max_length=250)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    porcentagem_cliente = models.IntegerField(blank=True, null=True)
    porcentagem_favorecido = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_distribuidores'


class DashboardDividaativa(models.Model):
    nome = models.CharField(max_length=150)
    valor_devido = models.CharField(max_length=20)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_dividaativa'


class DashboardDomiciliobancario(models.Model):
    banco = models.CharField(max_length=4)
    agencia = models.IntegerField()
    conta = models.IntegerField()
    digito_conta = models.IntegerField()
    tipo_conta = models.CharField(max_length=10)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_domiciliobancario'


class DashboardEmailslojista(models.Model):
    endereco = models.CharField(max_length=250)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_emailslojista'


class DashboardEmpresas(models.Model):
    cnpj = models.CharField(max_length=16)
    nome_empresarial = models.CharField(max_length=150)
    tipo_relacionamento = models.CharField(max_length=20)
    cargo = models.CharField(max_length=20)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_empresas'


class DashboardEnderecolojista(models.Model):
    principal = models.BooleanField(blank=True, null=True)
    cidade = models.CharField(max_length=50)
    estado = models.CharField(max_length=2)
    numero = models.CharField(max_length=10)
    cep = models.CharField(max_length=9)
    complemento = models.CharField(max_length=250)
    logradouro = models.CharField(max_length=250)
    bairro = models.CharField(max_length=250)
    tipo = models.CharField(max_length=250)
    pessoas_endereco = models.CharField(max_length=3)
    utilizado_pedido = models.BooleanField()
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey('DashboardLojista', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_enderecolojista'


class DashboardGrafiascnpj(models.Model):
    grafia = models.CharField(max_length=250)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    cnpj = models.ForeignKey(DashboardCnpj, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_grafiascnpj'


class DashboardIdwall(models.Model):

    class Meta:
        managed = False
        db_table = 'dashboard_idwall'


class DashboardItemvalidacoes(models.Model):
    regra = models.CharField(max_length=250, blank=True, null=True)
    nome = models.CharField(max_length=250, blank=True, null=True)
    descricao = models.CharField(max_length=250, blank=True, null=True)
    resultado = models.CharField(max_length=250, blank=True, null=True)
    mensagem = models.CharField(max_length=250, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    validacao = models.ForeignKey('DashboardValidacao', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_itemvalidacoes'


class DashboardLiquidacao(models.Model):
    dataautorizacao = models.DateField()
    codautorizacao = models.CharField(max_length=40, blank=True, null=True)
    nsu = models.IntegerField(blank=True, null=True)
    acquirer_nsu = models.CharField(max_length=40, blank=True, null=True)
    terminal = models.CharField(max_length=50, blank=True, null=True)
    produto = models.CharField(max_length=40, blank=True, null=True)
    bandeira = models.CharField(max_length=40, blank=True, null=True)
    valortransacao = models.FloatField()
    nro_parcela = models.TextField(blank=True, null=True)
    valorbruto = models.FloatField()
    taxaadministrativo = models.FloatField()
    valoradministrativo = models.FloatField()
    valorliquido = models.FloatField()
    mcc = models.IntegerField(blank=True, null=True)
    cpf_cnpj = models.CharField(max_length=20, blank=True, null=True)
    razao_nome = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=40, blank=True, null=True)
    dataprevista = models.DateField()
    datapagamento = models.DateField(blank=True, null=True)
    distribuidor = models.ForeignKey(DashboardDistribuidores, models.DO_NOTHING)
    cod_produto = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_liquidacao'


class DashboardLojista(models.Model):
    lojista_id = models.IntegerField(blank=True, null=True)
    nome = models.CharField(max_length=200)
    documento = models.CharField(unique=True, max_length=20)
    branch_id = models.CharField(max_length=20, blank=True, null=True)
    merchant_id = models.CharField(max_length=20, blank=True, null=True)
    affiliation_key = models.CharField(max_length=250, blank=True, null=True)
    mensagem_idwall = models.CharField(max_length=250, blank=True, null=True)
    matriz_idwall = models.CharField(max_length=250, blank=True, null=True)
    numero_idwall = models.CharField(max_length=300, blank=True, null=True)
    resultado_idwall = models.CharField(max_length=250, blank=True, null=True)
    status_idwall = models.CharField(max_length=20, blank=True, null=True)
    pep_idwall = models.BooleanField(blank=True, null=True)
    fase = models.TextField()
    com_protesto = models.BooleanField(blank=True, null=True)
    operadora = models.CharField(max_length=250, blank=True, null=True)
    departamento = models.IntegerField()
    aprovacao_manual = models.BooleanField(blank=True, null=True)
    email_vendedor_responsavel = models.CharField(max_length=250, blank=True, null=True)
    relatorio_a_partir_pj = models.BooleanField(blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    agencia_favorecido = models.CharField(max_length=250, blank=True, null=True)
    banco_favorecido = models.IntegerField(blank=True, null=True)
    conta_favorecido = models.CharField(max_length=250, blank=True, null=True)
    digito_conta_favorecido = models.CharField(max_length=250, blank=True, null=True)
    porcentagem_favorecido = models.CharField(max_length=250, blank=True, null=True)
    tipo_conta_favorecido = models.CharField(max_length=250, blank=True, null=True)
    mensagem_integrador = models.TextField(blank=True, null=True)
    num_terminais = models.IntegerField()
    observacao_liberacao_risco = models.CharField(max_length=1040, blank=True, null=True)
    porcentagem_cliente = models.CharField(max_length=250, blank=True, null=True)
    cnpj_favorecido = models.CharField(max_length=250, blank=True, null=True)
    nome_razao_favorecido = models.CharField(max_length=250, blank=True, null=True)
    plano = models.ForeignKey('DashboardPlanos', models.DO_NOTHING, blank=True, null=True)
    distribuidor = models.ForeignKey(DashboardDistribuidores, models.DO_NOTHING, blank=True, null=True)
    fase_integracao = models.CharField(max_length=30, blank=True, null=True)
    area_repsonsavel = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_lojista'


class DashboardLojistaterminal(models.Model):
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista_id = models.ForeignKey(DashboardLojista, models.DO_NOTHING)
    terminal_id = models.ForeignKey('DashboardTerminais', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_lojistaterminal'


class DashboardNomefonteconsulta(models.Model):
    nome = models.CharField(max_length=150)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    resumo_consulta = models.ForeignKey('DashboardResumoconsultas', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_nomefonteconsulta'


class DashboardNotificacoes(models.Model):
    titulo = models.CharField(max_length=250, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)
    lida = models.BooleanField(blank=True, null=True)
    link = models.CharField(max_length=450, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    destinatario = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_notificacoes'


class DashboardPagamento(models.Model):
    tipo_pagamento = models.CharField(max_length=25)
    parcelas = models.CharField(max_length=10, blank=True, null=True)
    nsu = models.CharField(max_length=250, blank=True, null=True)
    data_pagamento = models.DateField(blank=True, null=True)
    pagamento_valido = models.BooleanField()
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_pagamento'


class DashboardParticipacaoempresasitens(models.Model):
    cnpj = models.CharField(max_length=20, blank=True, null=True)
    nome_empresarial = models.CharField(max_length=150, blank=True, null=True)
    tipo_relacionamento = models.CharField(max_length=200, blank=True, null=True)
    cargo = models.CharField(max_length=150, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_participacaoempresasitens'


class DashboardPaystore(models.Model):

    class Meta:
        managed = False
        db_table = 'dashboard_paystore'


class DashboardPedido(models.Model):
    numero_pedido = models.IntegerField()
    status_pedido = models.CharField(max_length=250)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    comercial = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)
    pagamento = models.ForeignKey(DashboardPagamento, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_pedido'


class DashboardPessoasrelacionadas(models.Model):
    cpf = models.CharField(max_length=25)
    nome = models.CharField(max_length=300)
    tipo = models.CharField(max_length=20)
    cargo = models.CharField(max_length=20)
    cnpj = models.CharField(max_length=16)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_pessoasrelacionadas'


class DashboardPlanos(models.Model):
    nome = models.CharField(max_length=250)
    tipo_contratacao = models.CharField(max_length=250)
    valor_contratacao = models.FloatField()
    taxa_debito = models.FloatField()
    taxa_credito = models.FloatField()
    taxa_parcelado = models.FloatField()
    produto = models.ForeignKey('DashboardProdutos', models.DO_NOTHING)
    adquirencia = models.CharField(max_length=250)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    taxa_parcelado_sete_doze = models.FloatField()

    class Meta:
        managed = False
        db_table = 'dashboard_planos'


class DashboardProdutos(models.Model):
    descricao = models.CharField(max_length=250)
    identificador = models.CharField(max_length=250)
    gprs = models.BooleanField(blank=True, null=True)
    wifi = models.BooleanField(blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_produtos'


class DashboardProfile(models.Model):
    user = models.OneToOneField(AuthUser, on_delete=models.CASCADE)
    distribuidor = models.TextField()
    tipo_perfil = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'dashboard_profile'


class DashboardProtesto(models.Model):
    estado_protesto = models.CharField(max_length=2)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_protesto'


class DashboardQsacnpj(models.Model):
    nome = models.CharField(max_length=250)
    nome_representante = models.CharField(max_length=250, blank=True, null=True)
    pais_origem = models.CharField(max_length=250, blank=True, null=True)
    qualificacao = models.CharField(max_length=250)
    qualificacao_representante_legal = models.CharField(max_length=250, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    cnpj = models.ForeignKey(DashboardCnpj, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_qsacnpj'


class DashboardRegistration(models.Model):

    class Meta:
        managed = False
        db_table = 'dashboard_registration'


class DashboardResponsavel(models.Model):
    nome = models.CharField(max_length=250, blank=True, null=True)
    documento = models.CharField(max_length=20, blank=True, null=True)
    tipo = models.CharField(max_length=40, blank=True, null=True)
    data_nascimento = models.CharField(max_length=40, blank=True, null=True)
    sexo = models.CharField(max_length=1, blank=True, null=True)
    ddd = models.CharField(max_length=2, blank=True, null=True)
    telefone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    nacionalidade = models.CharField(max_length=20, blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_responsavel'


class DashboardResumoconsultas(models.Model):
    nome_matriz = models.CharField(max_length=150)
    status_protocolo = models.CharField(max_length=20)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_resumoconsultas'


class DashboardRetorno4Ward(models.Model):
    id_type = models.IntegerField()
    id_number = models.CharField(max_length=20)
    codigo_adquirente = models.IntegerField()
    status = models.IntegerField()
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista_id = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_retorno4ward'


class DashboardSintegraitem(models.Model):
    cnpj = models.CharField(max_length=20, blank=True, null=True)
    inscricao_estadual = models.CharField(max_length=20, blank=True, null=True)
    razao_social = models.CharField(max_length=250, blank=True, null=True)
    cnae_principal = models.CharField(max_length=250, blank=True, null=True)
    situacao_cadastral_vigente = models.CharField(max_length=20, blank=True, null=True)
    data_situacao_cadastral = models.DateField(blank=True, null=True)
    regime_apuracao = models.CharField(max_length=250, blank=True, null=True)
    data_credenciamento_nfe = models.CharField(max_length=250, blank=True, null=True)
    indicador_obrigatoriedade_nfe = models.CharField(max_length=250, blank=True, null=True)
    data_obrigatoriedade_nfe = models.CharField(max_length=250, blank=True, null=True)
    protocolo = models.CharField(max_length=250, blank=True, null=True)
    tipo = models.CharField(max_length=250, blank=True, null=True)
    uf = models.CharField(max_length=2)
    cad_icms = models.CharField(max_length=250, blank=True, null=True)
    cae = models.CharField(max_length=250, blank=True, null=True)
    nome_fantasia = models.CharField(max_length=250, blank=True, null=True)
    data_inicio_atividade = models.CharField(max_length=250, blank=True, null=True)
    natureza_juridica = models.CharField(max_length=250, blank=True, null=True)
    natureza_estabelecimento = models.CharField(max_length=250, blank=True, null=True)
    data_abertura = models.CharField(max_length=250, blank=True, null=True)
    data_baixa = models.CharField(max_length=250, blank=True, null=True)
    delegacia_fazendaria = models.CharField(max_length=250, blank=True, null=True)
    enquadramento_empresa = models.CharField(max_length=250, blank=True, null=True)
    observacao = models.CharField(max_length=250, blank=True, null=True)
    data_inscricao = models.CharField(max_length=250, blank=True, null=True)
    ped = models.CharField(max_length=250, blank=True, null=True)
    data_inicio_ped = models.CharField(max_length=250, blank=True, null=True)
    ultrapassou_limite_estadual = models.CharField(max_length=250, blank=True, null=True)
    data_inicio_simples_nacional = models.CharField(max_length=250, blank=True, null=True)
    condicao = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    indicador_obrigatoriedade_efd = models.CharField(max_length=250, blank=True, null=True)
    data_obrigatoriedade_efd = models.CharField(max_length=250, blank=True, null=True)
    opcao_simples = models.CharField(max_length=250, blank=True, null=True)
    indicador_obrigatoriedade_cte = models.CharField(max_length=250, blank=True, null=True)
    data_obrigatoriedade_cte = models.CharField(max_length=250, blank=True, null=True)
    situacao_sintegra = models.CharField(max_length=250, blank=True, null=True)
    tipo_unidade_auxiliar = models.CharField(max_length=250, blank=True, null=True)
    regime_pagamento = models.CharField(max_length=250, blank=True, null=True)
    situacao_contribuinte = models.CharField(max_length=250, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_sintegraitem'


class DashboardSintegraitematividadesecundaria(models.Model):
    codigo = models.CharField(max_length=50, blank=True, null=True)
    descricao = models.CharField(max_length=150, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    sintegra_item = models.ForeignKey(DashboardSintegraitem, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_sintegraitematividadesecundaria'


class DashboardSkipper(models.Model):

    class Meta:
        managed = False
        db_table = 'dashboard_skipper'


class DashboardSlack(models.Model):

    class Meta:
        managed = False
        db_table = 'dashboard_slack'


class DashboardSoftwareexpress(models.Model):

    class Meta:
        managed = False
        db_table = 'dashboard_softwareexpress'


class DashboardTaxas(models.Model):
    credito_a_vista = models.FloatField()
    credito_parcelado_emissor = models.FloatField()
    debito = models.FloatField()
    parcelado_loja_dois_seis = models.FloatField()
    parcelado_loja_sete_doze = models.FloatField()
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_taxas'


class DashboardTelefones(models.Model):
    fone_ddd = models.CharField(max_length=2, blank=True, null=True)
    fone_numero = models.CharField(max_length=10, blank=True, null=True)
    principal = models.BooleanField()
    tipo_assinante = models.CharField(max_length=20, blank=True, null=True)
    pessoas_com_o_mesmo_telefone = models.CharField(max_length=5, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_telefones'


class DashboardTentativasresumoconsulta(models.Model):
    duracao_tentativa = models.FloatField()
    hora_fim_tentativa = models.DateTimeField(blank=True, null=True)
    hora_inicio_tentativa = models.DateTimeField(blank=True, null=True)
    msg_erro_tentativa = models.CharField(max_length=250)
    status_fonte = models.CharField(max_length=10)
    status_tentativa = models.CharField(max_length=25)
    tipo_erro_tentativa = models.CharField(max_length=250, blank=True, null=True)
    fonte = models.ForeignKey(DashboardNomefonteconsulta, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_tentativasresumoconsulta'


class DashboardTerminais(models.Model):
    terminal_id_logical = models.CharField(max_length=50)
    terminal_id_physical = models.CharField(max_length=50, blank=True, null=True)
    tecnology_code = models.IntegerField()
    terminal_serial_number = models.CharField(max_length=50, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    nota_gerada = models.BooleanField(blank=True, null=True)
    token_otp = models.CharField(max_length=50)
    token_data_criacao = models.DateTimeField(blank=True, null=True)
    token_data_expiracao = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_terminais'


class DashboardTransacoes(models.Model):
    nsu = models.IntegerField(blank=True, null=True)
    acquirer_nsu = models.CharField(unique=True, max_length=40, blank=True, null=True)
    value = models.FloatField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    parcels = models.IntegerField(blank=True, null=True)
    brand = models.CharField(max_length=40, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    finish_date = models.DateTimeField(blank=True, null=True)
    confirmation_date = models.DateTimeField(blank=True, null=True)
    payment_date = models.DateTimeField(unique=True, blank=True, null=True)
    response_code = models.CharField(max_length=10, blank=True, null=True)
    response_message = models.CharField(max_length=250, blank=True, null=True)
    authorization_number = models.CharField(max_length=40, blank=True, null=True)
    terminal = models.CharField(max_length=40, blank=True, null=True)
    tef_terminal = models.CharField(max_length=40, blank=True, null=True)
    terminal_serial_number = models.CharField(max_length=40, blank=True, null=True)
    terminal_manufacturer = models.CharField(max_length=40, blank=True, null=True)
    terminal_model = models.CharField(max_length=40, blank=True, null=True)
    terminal_type = models.CharField(max_length=40, blank=True, null=True)
    acquirer = models.CharField(max_length=40, blank=True, null=True)
    merchant = models.CharField(max_length=40, blank=True, null=True)
    tef_merchant = models.CharField(max_length=40, blank=True, null=True)
    merchant_category_code = models.CharField(max_length=40, blank=True, null=True)
    merchant_national_type = models.CharField(max_length=40, blank=True, null=True)
    merchant_national_id = models.CharField(max_length=40, blank=True, null=True)
    product_name = models.CharField(max_length=40, blank=True, null=True)
    product_id = models.CharField(max_length=40, blank=True, null=True)
    card_input_method = models.CharField(max_length=40, blank=True, null=True)
    requested_password = models.CharField(max_length=40, blank=True, null=True)
    fallback = models.CharField(max_length=40, blank=True, null=True)
    origin = models.CharField(max_length=40, blank=True, null=True)
    authorization_time = models.IntegerField(blank=True, null=True)
    client_version = models.CharField(max_length=250, blank=True, null=True)
    server_version = models.CharField(max_length=250, blank=True, null=True)
    adquirente = models.CharField(max_length=20, blank=True, null=True)
    simcard_provider = models.CharField(max_length=40, blank=True, null=True)
    simcard_serial_number = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_transacoes'
        unique_together = (('acquirer_nsu', 'payment_date', 'authorization_number'),)


class DashboardTransacoeselastic(models.Model):
    value = models.FloatField(blank=True, null=True)
    parcels = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    brand = models.CharField(max_length=40, blank=True, null=True)
    product_name = models.CharField(max_length=40, blank=True, null=True)
    product_id = models.IntegerField(blank=True, null=True)
    payment_date = models.CharField(max_length=40, blank=True, null=True)
    merchant_name = models.CharField(max_length=250, blank=True, null=True)
    merchant_national_id = models.CharField(max_length=40, blank=True, null=True)
    supplier_name = models.CharField(max_length=250, blank=True, null=True)
    supplier_national_id = models.CharField(max_length=40, blank=True, null=True)
    nsu = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dashboard_transacoeselastic'


class DashboardValidacao(models.Model):
    mensagem = models.CharField(max_length=250, blank=True, null=True)
    nome = models.CharField(max_length=250, blank=True, null=True)
    resultado = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=250, blank=True, null=True)
    data_cadastro = models.DateTimeField(blank=True, null=True)
    data_update = models.DateTimeField(blank=True, null=True)
    lojista = models.ForeignKey(DashboardLojista, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dashboard_validacao'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DumpCredenciamento(models.Model):
    field_field = models.TextField(db_column='\ufeff--', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'dump_credenciamento'


class LojistaAtivacaoterminal(models.Model):
    communication_profile = models.CharField(max_length=200)
    merchant = models.CharField(max_length=10)
    terminal = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'lojista_ativacaoterminal'


class LojistaPaystore(models.Model):

    class Meta:
        managed = False
        db_table = 'lojista_paystore'


class Tparcelas(models.Model):
    nroparcela = models.IntegerField(blank=True, null=True)
    nroparcela_final = models.IntegerField(blank=True, null=True)
    desc_referencia = models.TextField(blank=True, null=True)
    nro_incremento = models.IntegerField(blank=True, null=True)
    idmodalidade = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tparcelas'


class Ttmp(models.Model):
    data = models.DateField(primary_key=True)
    nr_dia = models.IntegerField(blank=True, null=True)
    tt_dia_mes = models.IntegerField(blank=True, null=True)
    nr_mes = models.IntegerField(blank=True, null=True)
    nr_ano = models.IntegerField(blank=True, null=True)
    dt_dia_util_anterior = models.DateField(blank=True, null=True)
    dt_proximo_dia_util = models.DateField(blank=True, null=True)
    fl_dia_util = models.IntegerField(blank=True, null=True)
    fl_dia_util_incluindo_sabado = models.IntegerField(blank=True, null=True)
    fl_feriado = models.IntegerField(blank=True, null=True)
    nr_dia_semana = models.IntegerField(blank=True, null=True)
    ds_dia_semana = models.TextField(blank=True, null=True)
    nr_semana = models.IntegerField(blank=True, null=True)
    nr_semana_mes = models.IntegerField(blank=True, null=True)
    nr_dia_ano = models.IntegerField(blank=True, null=True)
    qt_dias_uteis_mes = models.IntegerField(blank=True, null=True)
    tt_dias_uteis_mes = models.IntegerField(blank=True, null=True)
    qt_dias_uteis_ano = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ttmp'


class AgendaFinanceira(models.Model):
    dataautorizacao = models.DateField()
    codautorizacao = models.CharField(max_length=40, blank=True, null=True)
    nsu = models.IntegerField(primary_key=True)
    terminal = models.CharField(max_length=40, blank=True, null=True)
    produto = models.CharField(max_length=250, blank=True, null=True)
    bandeira = models.CharField(max_length=40, blank=True, null=True)
    valortransacao = models.FloatField()
    nro_parcela = models.TextField(blank=True, null=True)
    parcela = models.IntegerField(blank=True, null=True)
    valorbruto = models.FloatField()
    taxaadministrativo = models.FloatField()
    valoradministrativo = models.FloatField()
    valorliquido = models.FloatField()
    cnpj_cpf = models.CharField(max_length=40, blank=True, null=True)
    razao_nome = models.CharField(max_length=250, blank=True, null=True)
    status = models.TextField(max_length=40, blank=True, null=True)
    dataprevista = models.DateField(blank=True, null=True)
    datapagamento = models.DateField(blank=True, null=True)
    flagcancelada = models.IntegerField(blank=True, null=True)
    distribuidor_id = models.IntegerField(blank=True, null=True)
    product_id = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agendafinanceira'


class AgendaSkipper(models.Model):
    vl_bruto = models.FloatField()
    vl_desc = models.FloatField()
    vl_antecip = models.FloatField()
    vl_liq = models.FloatField()
    nsu = models.CharField(max_length=40, blank=True, null=True)
    dt_trans = models.DateField(blank=True, null=True)
    parc = models.IntegerField(blank=True, null=True)
    total_parc = models.IntegerField(blank=True, null=True)
    dt_sched = models.DateField(blank=True, null=True)
    autoriz = models.CharField(max_length=40, blank=True, null=True)
    prod_trans = models.CharField(max_length=40, blank=True, null=True)
    bandeira = models.CharField(max_length=40, blank=True, null=True)
    lote_id = models.IntegerField(blank=True, null=True)
    data_pagto = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agenda_skipper'


class ConciliacaoSkipper(models.Model):
    vl_bruto = models.FloatField()
    vl_desc = models.FloatField()
    vl_antecip = models.FloatField()
    vl_liq = models.FloatField()
    nsu = models.CharField(max_length=40, blank=True, null=True)
    dt_trans = models.DateField(blank=True, null=True)
    parc = models.IntegerField(blank=True, null=True)
    total_parc = models.IntegerField(blank=True, null=True)
    dt_sched = models.DateField(blank=True, null=True)
    autoriz = models.CharField(max_length=40, blank=True, null=True)
    prod_trans = models.CharField(max_length=40, blank=True, null=True)
    bandeira = models.CharField(max_length=40, blank=True, null=True)
    lote_id = models.IntegerField(blank=True, null=True)
    data_pagto = models.DateField(blank=True, null=True)
    lojista = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'conciliacao_skipper'


from storages.backends.s3boto3 import S3Boto3Storage

class SecurityTokenWorkaroundS3Boto3Storage(S3Boto3Storage):
    def _get_security_token(self):
        return None

class MediaStorage(SecurityTokenWorkaroundS3Boto3Storage):
    location = 'media'
    file_overwrite = False

class DocumentoExcel(models.Model):
    arquivo = models.FileField(storage=MediaStorage())

    class Meta:
        managed = False
        db_table = 'api_documentoexcel'

